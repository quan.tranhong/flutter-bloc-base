import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vinhcine/generated/l10n.dart';
import 'package:vinhcine/models/entities/token_entity.dart';
import 'package:vinhcine/models/entities/user_entity.dart';

import '../models/entities/index.dart';

part 'app_state.dart';

class AppCubit extends Cubit<AppState> {
  AppCubit() : super(WaitingForWarmingUp());
  late SharedPreferences prefs;

  void fetchData() async{
    var currentLocale = await getLanguageFromLocalStorage();
    var currentTheme = await getThemeFromLocalStorage();
    emit(FetchedFullDataSuccessfully(token: TokenEntity(), user: UserEntity(), currentLocale: currentLocale, currentTheme: currentTheme));
  }

  void fetchAppLanguage() async{
    emit(WaitingForFetchingLanguage());
    ///Language
    var currentLocale = await getLanguageFromLocalStorage();
    emit(FetchedLanguageSuccessfully(currentLocale: currentLocale));
  }

  void fetchAppTheme() async{
    emit(WaitingForFetchingTheme());
    ///Theme
    var currentTheme = await getThemeFromLocalStorage();
    emit(FetchedThemeSuccessfully(currentTheme: currentTheme));
  }

  Future<Locale> getLanguageFromLocalStorage() async{
    prefs = await SharedPreferences.getInstance();
    ///Language
    String languageCode = prefs.getString('languageCode') ?? window.locale.languageCode;
    var locale = S.delegate.supportedLocales.firstWhere(
          (element) => element.languageCode == languageCode,
      orElse: () => Locale.fromSubtags(languageCode: 'en'),
    );
    return locale;
  }

  Future<ThemeMode> getThemeFromLocalStorage() async{
    prefs = await SharedPreferences.getInstance();
    ///Theme
    String themeModeCode = prefs.getString("themeCode") ?? ThemeMode.light.code;
    final themeMode = ThemeModeExtension.fromCode(themeModeCode);
    // currentThemeMode.value = themeMode;
    // String languageCode = prefs.getString('languageCode') ?? window.locale.languageCode;
    // var locale = S.delegate.supportedLocales.firstWhere(
    //       (element) => element.languageCode == languageCode,
    //   orElse: () => Locale.fromSubtags(languageCode: 'en'),
    // );
    return themeMode;
  }

  void updateLocale(Locale locale){
    prefs.setString('languageCode', locale.languageCode);
    emit(ChangedLanguageSuccessfully(currentLocale: locale));
  }

  void updateTheme(ThemeMode themeMode) {
    prefs.setString('themeCode', themeMode.code);
    emit(ChangedThemeSuccessfully(currentTheme: themeMode));
  }

  // void updateToken(TokenEntity token) {
  //   emit(state.copyWith(token: token));
  // }
  //
  // void updateUser(TokenEntity token) {
  //   emit(state.copyWith(token: token));
  // }
}

extension ThemeModeExtension on ThemeMode {
  String get code {
    switch (this) {
      case ThemeMode.light:
        return 'light';
      case ThemeMode.dark:
        return 'dark';
      default:
        return 'system';
    }
  }

  static ThemeMode fromCode(String code) {
    switch (code) {
      case 'light':
        return ThemeMode.light;
      case 'dark':
        return ThemeMode.dark;
      default:
        return ThemeMode.system;
    }
  }
}
